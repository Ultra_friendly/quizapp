package com.bignerdranch.android.geoquiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class QuizActivity extends AppCompatActivity implements View.OnClickListener {
    private Button mTrueButton;
    private Button mFalseButton;
    private ImageButton mGtss;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        mTrueButton = (Button) findViewById(R.id.true_button);
        mFalseButton = (Button) findViewById(R.id.false_button);
        mGtss = (ImageButton) findViewById(R.id.mGtss);



        mGtss.setOnClickListener(this);
        mTrueButton.setOnClickListener(this);
        mFalseButton.setOnClickListener(this);
    }

        @Override
        public void onClick(View v){
            switch (v.getId()) {
                case R.id.mGtss:
                    Intent intent = new Intent(this, StartActivity.class);
                    startActivity(intent);
                    break;
                case R.id.true_button :
                    Toast.makeText(QuizActivity.this, R.string.incorrect_toast, Toast.LENGTH_SHORT).show();
                    break;
                case R.id.false_button:
                    Toast.makeText(QuizActivity.this, R.string.correct_toast, Toast.LENGTH_SHORT).show();
                    break;
                default:
                    break;

            }

        }
    }
